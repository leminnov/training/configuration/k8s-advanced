# TODO WIP

#!/bin/bash 

echo "#####################################"
echo "                APT "
echo "#####################################"

sudo apt update
sudo apt install -y wget unzip gcc jq build-essential bat tree

echo "#####################################"
echo "                GO "
echo "#####################################"

wget https://golang.org/dl/go1.16.7.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.16.7.linux-amd64.tar.gz

echo '# Adding go to the path environment
export PATH=$PATH:/usr/local/go/bin/
export GOPATH=$HOME/go' >> ~/.bashrc

source ~/.bashrc
go version

echo "#####################################"
echo "                CFSSL "
echo "#####################################"

go get -u github.com/cloudflare/cfssl/cmd/cfssl
go get -u github.com/cloudflare/cfssl/cmd/cfssljson

echo '# Adding CFSSL
export PATH=$PATH:~/go/bin/' >> ~/.bashrc

source ~/.bashrc
cfssl version

echo "#####################################"
echo "                RKE "
echo "#####################################"

sudo mkdir /opt/rke
sudo wget https://github.com/rancher/rke/releases/download/v1.1.19/rke_linux-amd64
sudo mv rke_linux-amd64 /opt/rke/rke
sudo chmod +x /opt/rke/rke

echo '# Adding rke to the path environment
export PATH=$PATH:/opt/rke' >> ~/.bashrc

source ~/.bashrc
rke --version

echo "#####################################"
echo "                KUBECTL "
echo "#####################################"

sudo snap install kubectl --classic
kubectl version

echo "#####################################"
echo "                HELM3 "
echo "#####################################"

sudo snap install helm --classic
helm version