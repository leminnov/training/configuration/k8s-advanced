#!/bin/bash


echo "#####################################"
echo "       Docker Install Bastion Node"
echo "#####################################"

curl https://releases.rancher.com/install-docker/19.03.9.sh | sh
sudo usermod -aG docker ubuntu

docker version

echo "#####################################"
echo "       Docker Install Master Node"
echo "#####################################"


ssh -o "StrictHostKeyChecking no" -i key ubuntu@master

sudo hostnamectl set-hostname master
curl https://releases.rancher.com/install-docker/19.03.9.sh | sh
sudo usermod -aG docker ubuntu

docker version
